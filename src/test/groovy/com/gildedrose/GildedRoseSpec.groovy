package com.gildedrose

import spock.lang.Specification


/**
 * Spock unit tests.
 */
class GildedRoseSpec extends Specification {

	def "once the sell by date has passed, quality degrades twice as fast"() {
		given:
		Item item = new Item("Test Item", 2, 10)
		Item[] items = [item]
		def gildedRose = new GildedRose(items)
		def itemQualities = []
		when:
		4.times {
			gildedRose.updateQuality()
			itemQualities << item.quality
		}
		then:
		itemQualities == [9, 8, 6, 4]
	}

	def "the quality of an item is never negative"() {
		given:
		Item item = new Item("Test Item", 10, 2)
		Item[] items = [item]
		def gildedRose = new GildedRose(items)
		when:
		5.times {
			gildedRose.updateQuality()
		}
		then:
		item.quality == 0
	}

	def "aged brie increases in quality as it gets older"() {
		given:
		Item brie = new Item(SpecialItemTypes.AGED_BRIE, 10, 2)
		Item[] items = [brie]
		def gildedRose = new GildedRose(items)
		def itemQualities = []
		when:
		5.times {
			gildedRose.updateQuality()
			itemQualities << brie.quality
		}
		then:
		itemQualities == [3, 4, 5, 6, 7]
	}

	def "the quality of an item is never more than 50"() {
		given:
		Item brie = new Item(SpecialItemTypes.AGED_BRIE, 10, 49)
		def gildedRose = new GildedRose([brie] as Item[])
		def itemQualities = []
		when:
		5.times {
			gildedRose.updateQuality()
			itemQualities << brie.quality
		}
		then:
		itemQualities == [50, 50, 50, 50, 50]
	}

	def "the quality of sulfuras is always 80"() {
		given:
		Item sulfuras = new Item(SpecialItemTypes.SULFURAS, 2, 80)
		def gildedRose = new GildedRose([sulfuras] as Item[])
		when:
		gildedRose.updateQuality()
		then:
		sulfuras.quality == 80
	}

	def "sulfuras never has to be sold or decreases in quality"() {
		given:
		Item sulfuras = new Item(SpecialItemTypes.SULFURAS,  2,  80)
		def gildedRose = new GildedRose([sulfuras] as Item[])
		when:
		10.times {
			gildedRose.updateQuality()
		}
		then:
		sulfuras.quality == 80
		sulfuras.sellIn == 2
	}

	def "backstage passes increase in quality as the SellIn date approaches"() {
		given:
		int initialQuality = 10
		Item backstagePass = new Item(SpecialItemTypes.BACKSTAGE_PASS, 10, initialQuality)
		def gildedRose = new GildedRose([backstagePass] as Item[])
		when:
		8.times {
			gildedRose.updateQuality()
		}
		then:
		backstagePass.quality > initialQuality
	}

	def "backstage passes quality increases by 2 when there are 10 days or less"() {
		given:
		int initialQuality = 10
		Item backstagePass = new Item(SpecialItemTypes.BACKSTAGE_PASS, 12, initialQuality)
		def gildedRose = new GildedRose([backstagePass] as Item[])
		def qualities = []
		when:
		5.times {
			gildedRose.updateQuality()
			qualities << backstagePass.quality
		}
		then:
		qualities == [11, 12, 14, 16, 18]
	}

	def "backstage passes quality increases by 3 when there are 5 days or less"() {
		given:
		int initialQuality = 10
		Item backstagePass = new Item(SpecialItemTypes.BACKSTAGE_PASS, 7, initialQuality)
		def gildedRose = new GildedRose([backstagePass] as Item[])
		def qualities = []
		when:
		5.times {
			gildedRose.updateQuality()
			qualities << backstagePass.quality
		}
		then:
		qualities == [12, 14, 17, 20, 23]
	}

	def 'backstage passes quality drops to 0 after the concert'() {
		given:
		Item backstagePass = new Item(SpecialItemTypes.BACKSTAGE_PASS, 1, 10)
		def gildedRose = new GildedRose([backstagePass] as Item[])
		def qualities = []
		when:
		5.times {
			gildedRose.updateQuality()
			qualities << backstagePass.quality
		}
		then:
		qualities == [13, 0, 0, 0, 0]
	}

	def "conjured items degrade in quality twice as fast as normal items"() {
		given:
		Item normalItem = new Item("Normal Item", 10, 10)
		Item conjuredItem = new Item(SpecialItemTypes.getConjuredItem("item"), 10, 10)
		def gildedRose = new GildedRose([normalItem, conjuredItem] as Item[])
		def normalQualities = []
		def conjuredQualities = []
		when:
		5.times {
			gildedRose.updateQuality()
			normalQualities << normalItem.quality
			conjuredQualities << conjuredItem.quality
		}
		then:
		normalQualities == [9, 8, 7, 6, 5]
		conjuredQualities == [8, 6, 4, 2, 0]
	}
}

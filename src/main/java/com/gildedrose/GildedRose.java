package com.gildedrose;

import com.gildedrose.chain.UpdateQualityChain;
import com.gildedrose.handler.*;
import java.util.Arrays;

class GildedRose {
  private final Item[] items;
  private final UpdateQualityChain updateQualityChain;

  public GildedRose(final Item[] items) {
    this.items = items;
    this.updateQualityChain = new UpdateQualityChain();
  }

  public void updateQuality() {
    Arrays.stream(items).forEach(updateQualityChain::updateQuality);
  }
}

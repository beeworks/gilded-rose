package com.gildedrose.chain;

import com.gildedrose.Item;
import com.gildedrose.constraints.QualityConstraint;
import com.gildedrose.constraints.QualityIsNeverMoreThan50Constraint;
import com.gildedrose.constraints.QualityIsNeverNegativeConstraint;
import com.gildedrose.constraints.SulfurasQualityIsAlways80Constraint;
import com.gildedrose.handler.*;
import java.util.Arrays;

public class UpdateQualityChain {
  private static final QualityHandler[] qualityHandlers = {
    new DefaultQualityDecreaseHandler(),
    new AgedBrieImprovesWithAgeQualityHandler(),
    new BackstagePassQualityHandler(),
    new ConjuredQualityDecreaseHandler(),
    new DefaultSellInDatePassedQualityDecreaseHandler()
  };

  private static final SellInHandler[] sellInHandlers = {new DefaultSellInHandler()};

  private static final QualityConstraint[] qualityConstraints = {
    new QualityIsNeverMoreThan50Constraint(),
    new QualityIsNeverNegativeConstraint(),
    new SulfurasQualityIsAlways80Constraint()
  };

  public void updateQuality(final Item item) {
    applyQualityHandlers(item);
    applySellInHandlers(item);
    applyConstraints(item);
  }

  private void applyQualityHandlers(final Item item) {
    int qualityDecrease = 0;
    for (QualityHandler qualityHandler : qualityHandlers) {
      qualityDecrease = qualityHandler.getQualityDecrease(item, qualityDecrease);
    }
    item.quality = item.quality - qualityDecrease;
  }

  private void applySellInHandlers(final Item item) {
    Arrays.stream(sellInHandlers).forEach(sellInHandler -> sellInHandler.apply(item));
  }

  private void applyConstraints(final Item item) {
    Arrays.stream(qualityConstraints).forEach(qualityConstraint -> qualityConstraint.apply(item));
  }
}

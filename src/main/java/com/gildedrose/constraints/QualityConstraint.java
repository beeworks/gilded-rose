package com.gildedrose.constraints;

import com.gildedrose.Item;

public interface QualityConstraint {
  boolean appliesTo(Item item);

  void apply(Item item);
}

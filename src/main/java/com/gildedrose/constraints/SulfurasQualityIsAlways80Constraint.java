package com.gildedrose.constraints;

import com.gildedrose.Item;
import com.gildedrose.SpecialItemTypes;

public class SulfurasQualityIsAlways80Constraint implements QualityConstraint {
  @Override
  public boolean appliesTo(Item item) {
    return SpecialItemTypes.SULFURAS.equals(item.name);
  }

  @Override
  public void apply(Item item) {
    if (appliesTo(item)) item.quality = 80;
  }
}

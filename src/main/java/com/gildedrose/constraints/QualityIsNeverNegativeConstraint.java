package com.gildedrose.constraints;

import com.gildedrose.Item;

public class QualityIsNeverNegativeConstraint implements QualityConstraint {
  @Override
  public boolean appliesTo(Item item) {
    return true;
  }

  @Override
  public void apply(Item item) {

    if (appliesTo(item) && item.quality < 0) item.quality = 0;
  }
}

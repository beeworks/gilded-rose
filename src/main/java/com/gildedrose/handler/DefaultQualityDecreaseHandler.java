package com.gildedrose.handler;

import com.gildedrose.Item;
import com.gildedrose.SpecialItemTypes;

public class DefaultQualityDecreaseHandler implements QualityHandler {
  @Override
  public boolean appliesTo(Item item) {
    if (SpecialItemTypes.SULFURAS.equals(item.name)) return false;
    if (SpecialItemTypes.AGED_BRIE.equals(item.name)) return false;
    if (SpecialItemTypes.BACKSTAGE_PASS.equals(item.name)) return false;
    return true;
  }

  @Override
  public int getQualityDecrease(Item item, int currentQualityDecrease) {
    if (!appliesTo(item)) return currentQualityDecrease;
    return currentQualityDecrease + 1;
  }
}

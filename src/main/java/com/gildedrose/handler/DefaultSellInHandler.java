package com.gildedrose.handler;

import com.gildedrose.Item;
import com.gildedrose.SpecialItemTypes;

public class DefaultSellInHandler implements SellInHandler {

  @Override
  public boolean appliesTo(Item item) {
    return !SpecialItemTypes.SULFURAS.equals(item.name);
  }

  @Override
  public void apply(Item item) {
    if (appliesTo(item)) item.sellIn--;
  }
}

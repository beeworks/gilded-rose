package com.gildedrose.handler;

import com.gildedrose.Item;

public interface SellInHandler {
  boolean appliesTo(Item item);

  void apply(Item item);
}

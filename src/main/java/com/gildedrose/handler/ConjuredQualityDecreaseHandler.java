package com.gildedrose.handler;

import com.gildedrose.Item;
import com.gildedrose.SpecialItemTypes;

public class ConjuredQualityDecreaseHandler implements QualityHandler {
  @Override
  public boolean appliesTo(Item item) {
    return SpecialItemTypes.isConjuredItem(item);
  }

  @Override
  public int getQualityDecrease(Item item, int currentQualityDecrease) {
    if (!appliesTo(item)) return currentQualityDecrease;
    return currentQualityDecrease * 2;
  }
}

package com.gildedrose.handler;

import com.gildedrose.Item;
import com.gildedrose.SpecialItemTypes;

public class BackstagePassQualityHandler implements QualityHandler {

  @Override
  public boolean appliesTo(Item item) {
    return SpecialItemTypes.BACKSTAGE_PASS.equals(item.name);
  }

  @Override
  public int getQualityDecrease(Item item, int currentQualityDecrease) {
    if (!appliesTo(item)) return currentQualityDecrease;
    if (item.sellIn <= 0) return Integer.MAX_VALUE;
    else if (item.sellIn <= 5) return -3;
    else if (item.sellIn <= 10) return -2;
    else return -1;
  }
}

package com.gildedrose.handler;

import com.gildedrose.Item;
import com.gildedrose.SpecialItemTypes;

public class AgedBrieImprovesWithAgeQualityHandler implements QualityHandler {
  @Override
  public boolean appliesTo(Item item) {
    return SpecialItemTypes.AGED_BRIE.equals(item.name);
  }

  @Override
  public int getQualityDecrease(Item item, int currentQualityDecrease) {
    if (!appliesTo(item)) return currentQualityDecrease;
    return -1;
  }
}

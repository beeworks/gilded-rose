package com.gildedrose.handler;

import com.gildedrose.Item;

public interface QualityHandler {
  boolean appliesTo(Item item);

  int getQualityDecrease(Item item, int currentQualityDecrease);
}

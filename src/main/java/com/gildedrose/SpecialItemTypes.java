package com.gildedrose;

public class SpecialItemTypes {
  public static final String AGED_BRIE = "Aged Brie";
  public static final String BACKSTAGE_PASS = "Backstage passes to a TAFKAL80ETC concert";
  public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
  public static final String CONJURED = "Conjured";

  public static final String getConjuredItem(final String item) {
    return CONJURED + " " + item;
  }

  public static final boolean isConjuredItem(final Item item) {
    return item.name.startsWith(CONJURED);
  }
}
